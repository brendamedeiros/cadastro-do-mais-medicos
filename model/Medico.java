package model;

public class Medico extends Usuario{
	private String crm;
	private String especificacao;
	private Consulta consulta;
	
	// Método Construtores
	public Medico(String nome, String crm) {
		super(nome);
		this.crm = crm;
	}
	
	public Medico() {
		super();
	}

	// Métodos GET e SET
	public String getCrm() {
		return crm;
	}
	public void setCrm(String crm) {
		this.crm = crm;
	}
	public String getEspecificacao() {
		return especificacao;
	}
	public void setEspecificacao(String especificacao) {
		this.especificacao = especificacao;
	}
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
	

}
