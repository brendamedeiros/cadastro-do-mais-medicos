package model;

public class Consulta {
	private String descricao;
	private String remedio;
	
	
	// Métodos Construtores
	public Consulta(String descricao) {
		this.descricao = descricao;
	}

	public Consulta() {
	}

	// Métodos GET e SET
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getRemedio() {
		return remedio;
	}

	public void setRemedio(String remedio) {
		this.remedio = remedio;
	}
	
	
	
}
